import React from "react";
import Avatar from '@mui/material/Avatar';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Badge from '@mui/material/Badge';


export function Pokemon(props) {
  const pokemon = props.pokemon;
  const id = pokemon.entry_number;
  const name = pokemon.pokemon_species.name;
  const text = <h1>{name.charAt(0).toUpperCase() + name.slice(1)}</h1> 
  const sprite = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + id + ".png"

  return (
    <>
    <ListItem>
      <ListItemButton>
      <Badge 
          badgeContent={id} 
          color="success"
          max={999}
          overlap="rectangular"	
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
        >
        <ListItemAvatar>
          <Avatar
            alt={name}
            src={sprite}
            sx={{ width: 80, height: 80 }}
          />
        </ListItemAvatar>
        </Badge>
        
          <ListItemText id={id} primary={text} />
      </ListItemButton>
    </ListItem>
    </>
  )
}