import React, { useEffect } from "react";
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import List from '@mui/material/List';

import { Pokemon } from "./Pokemon.js";
import { useLocalStorage } from "./useLocalStorage.js";


export default function Pokedex() {
  const [pokedex, setPokedex] = useLocalStorage('pokedex', {});

  useEffect(() => {
    if ( Object.keys(pokedex).length === 0) {
      const fetchData = () => {
        const corsProxy = "https://dry-lowlands-97275.herokuapp.com/"
        const apiURL = "https://pokeapi.co/api/v2/"
        fetch(corsProxy + apiURL + "/pokedex/2")
        .then(response => {
          return response.json()
        })
        .then(data => {
          console.log("data: ", data.pokemon_entries);
          setPokedex(data.pokemon_entries);
        })
      }
    
      fetchData();
    }    
  })

  return (
    <Box sx={{
      display: 'flex',
      flexWrap: 'wrap',
      '& > :not(style)': {
          width: '100%',
          height: '100%',
          marginLeft: '40%',
          marginRight: '40%',
          marginTop: '2%',
          marginBottom: '1%',
      },}}>
      <Paper>
          <List 
            sx={{ 
              width: '100%', 
              maxWidth: 600, 
              bgcolor: '#C1BEBE',
              position: 'relative',
              '& ul': { padding: 0 },
              }}>
          {Object.keys(pokedex).length > 0 ? 
            pokedex.map((pokemon, i) =>
            <Pokemon key={i} pokemon={pokemon} />
            )
            : <></>
          }
          </List>
      </Paper>
    </Box>
  )
}