import React from 'react'
import Pokedex from './Pokedex';

export default function App() {
  return (
    <div>
      <Pokedex />
    </div>
    
  );
}

